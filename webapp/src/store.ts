import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

interface LoginCredentials {
    username: string;
    password: string;
}

@Module
class Authentication extends VuexModule {
    private authorizationHeader = '';

    @Mutation
    public setAuthorizationHeader(newHeader: string) {
        this.authorizationHeader = newHeader;
    }

    @Action
    public async setLoginCredentials(payload: LoginCredentials) {
        const authHeader = 'Basic ' + window.btoa(payload.username + ':' + payload.password);
        // keep this in sync
        this.context.commit('setAuthorizationHeader', authHeader);
        axios.defaults.headers.common.Authorization = authHeader;
    }

    @Action
    public async clearLoginCredentials() {
        // keep this in sync
        this.context.commit('setAuthorizationHeader', '');
        axios.defaults.headers.common.Authorization = null;
    }

    get authHeader(): string {
        return this.authorizationHeader;
    }

    get hasAuthentication(): boolean {
        const str = this.authorizationHeader;
        return (!str || 0 === str.length);
    }
}

export default new Vuex.Store({
    strict: debug,
    modules: {
        Authentication,
    },
});
