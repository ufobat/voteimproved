import Vue from 'vue';
import axios from 'axios';
import router from './router';
import store from './store';
import App from './App.vue';

Vue.config.productionTip = false;
axios.defaults.headers.common.Accept = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json';

axios.interceptors.response.use((response) => {
    return response;
}, (error) => {

    if ( !(error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false)) {
        // no manual error handling?
        // okay lets log out from backend
        window.console.log('error from api, logging out...');
        window.console.log(error.response);
        store.dispatch('clearLoginCredentials');
    }

    return Promise.reject(error);
});


new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
