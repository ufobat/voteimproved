use v6.c;
use Cro::HTTP::Router;
use Cro::HTTP::Auth::Basic;
use UUID;
use Log;

class MyUser does Cro::HTTP::Auth {
    has $.username;
}
subset LoggedInUser of MyUser where { .username.defined }

class MyBasicAuth does Cro::HTTP::Auth::Basic[MyUser, "username"] {
    method authenticate(Str $user, Str $pass --> Bool) {
        # No, don't actually do this!
        say "authentication called";
        my $success = $user eq 'admin' && $pass eq 'secret';
        return $success
    }
}

sub routes() is export {
    my %storage;
    route {
        before MyBasicAuth.new;
        # before semantics will change in cro 0.8.0.
        # delegate <*> is not required in the future
        # https://stackoverflow.com/questions/53062118/basic-authentication-in-perl6-with-cro
        delegate <*> => auth-routes();
    }
}

sub auth-routes() {
    my $log = Log.new;
    my %storage;

    route {
        get -> 'api', 'ping', $name {
            content 'text/plain', "echo $name";
        }
        get -> LoggedInUser $user, 'api', $uuid {
            $log.trace("api GET $uuid");
            content 'application/json', %storage{$uuid};
        }
        post -> LoggedInUser $user, 'api' {
            request-body -> %json-object {
                my $uuid = UUID.new(:version(4));
                %storage{$uuid} = %json-object;
                $log.trace("api POST $uuid");
                created "api/$uuid", 'application/json', %json-object;
            }
        }
    }
}
