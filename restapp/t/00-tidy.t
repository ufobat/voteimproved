use v6.c;
use lib 'lib';

use Path::Iterator;
use Test;

# check for trailing spaces
# check for tabs
my @dirs = '.';
for Path::Iterator.skip-vcs.skip-dir('.precomp').ext(rx/ ^ ( 'p' <[lm]>? 6? | t ) $ /).in(@dirs) -> $file {
    check_tidy($file);
}
done-testing;

sub check_tidy($file) {
    #diag $file;
    my @lines = $file.IO.lines;
    my @spaces = @lines.grep(rx/\s$/);
    is-deeply @spaces, [], "spaces at the end of $file";

    my @tabs = @lines.grep(rx/\t/);
    is-deeply @tabs, [], "tabs in $file";
}
