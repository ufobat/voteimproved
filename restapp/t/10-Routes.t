use v6.c;

use Cro::HTTP::Test;
use Routes;

test-service routes(), {
    test get('/api/ping/foo'),
    status => 200,
    content-type => 'text/plain',
    body => / 'echo foo' /;

    test-given '/api', {
        test get(),
        status => 405;

        my $trueUri;
        my $falseUri;

        test post(json => { :json('true') }),
        status => 201,
        headers => {
            Location => sub { $trueUri = $^Location; return $^Location}
        },
        json => { :json("true") };

        test post(json => { :json('false') }),
        status => 201,
        headers => {
            Location => sub { $falseUri = $^Location; return $^Location}
        },
        json => { :json("false") };

        test get($falseUri),
        status => 200,
        json => { :json('false') };

        test get($trueUri),
        status => 200,
        json => { :json('true') };

    }
}

done-testing;
