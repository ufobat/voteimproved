# voteimproved

## Project setup
Install docker (community edition) and docker-compose
 * https://docs.docker.com/compose/install/
 * https://docs.docker.com/install/linux/docker-ce/ubuntu/

If you don't want to use `root` or `sudo` configure your system as described here:
* https://docs.docker.com/install/linux/linux-postinstall/

```
docker-compose build
docker-compose up -d
docker-compose logs [-f]
docker-compose down
```

## Backend worflow
Install deps
```
zef install --deps-only --/test .
```

Run test
```
zef test .
```

Run the server
```
cro run
```

## Frontend workflow
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
## Backend
### Run backend
Frontend should be build to serve it
```
cro run
```

## Dev workflow
1. Create a branch
2. commit
3. push
4. open pr
5. request review
6. fix problems
7. merge
